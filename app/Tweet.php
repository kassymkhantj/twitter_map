<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = array('id', 'long', 'lat', 'username', 'text', 'date_created');
    public $timestamps = false;

    protected $table = 'tweets';

    protected $connection = 'mysql';

    public function tags()
    {
    	return $this->belongsToMany('App\Tag', 'tweet_tag', 'tweet_id', 'tag_id');
    }

}
