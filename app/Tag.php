<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = array('text');
    public $timestamps = false;

    protected $connection = 'mysql';

    public function tweets()
    {
    	return $this->belongsToMany('App\Tweet', 'tweet_tag', 'tag_id', 'tweet_id');
    }
}
