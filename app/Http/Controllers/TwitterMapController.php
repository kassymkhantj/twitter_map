<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Twitter;
use Log;
use App\Tweet;
use App\Tag;

class TwitterMapController extends BaseController
{

	public function tweet_search($q=null)
	{	
		if(!$q){
			return json_encode(Tweet::all());
		}

		$res = Twitter::getSearch([
			'q'=>'#' . $q,
			'result_type'=>'recent',
			'geocode'=>'59.930391,30.314907,100km',
			'count'=>100
		]);
		$tweets = array();
		foreach ($res->statuses as $tweet) {
			if($tweet->coordinates!=null){
				Tweet::firstOrCreate(
					array('id'=>$tweet->id),
					array(
						'long'=>$tweet->coordinates->coordinates[0],
						'lat'=>$tweet->coordinates->coordinates[1],
						'username'=>$tweet->user->name,
						'text'=>$tweet->text,
						'date_created'=>date('d-m-y H:i:s', strtotime($tweet->created_at))
					)
				);
				foreach ($tweet->entities->hashtags as $tag) {
					$tag = Tag::firstOrCreate(array('text'=>$tag->text));
					$tag->tweets()->attach($tweet->id);
				}
			}
		}
		$tagId = Tag::where('text', '=', $q)->first();
		if(!$tagId){
			return json_encode(array());
		}
		return json_encode(Tweet::join('tweet_tag', 'id', '=', 'tweet_id')->where('tweet_tag.tag_id', '=', $tagId->id)->get());
	}
}
