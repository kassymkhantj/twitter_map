<h1>{{$len}}</h1>
@foreach ($res as $tweet)
	@if($tweet!=null)
		<p>{{ $tweet->text }}</p>
	    <p>{{ $tweet->user->location }}</p>
	    @if($tweet->coordinates!=null)
	    	<p>{{ $tweet->coordinates->coordinates[0] }}</p>
	    	<p>{{ $tweet->coordinates->coordinates[1] }}</p>
	    @endif

	    @if($tweet->place!=null)
	    	<p>{{ $tweet->place->url }}</p>
	    @endif

	    @if($tweet->geo!=null)
	    	<p>{{ $tweet->geo->type }}</p>
	    @endif
	<hr>
    @endif
@endforeach
<div style="width: 500; height: 500">
	{!!Mapper::render()!!}
</div>