# Twitter map project

Prerequirements
* composer
* laravel
* mysql

Clone project:


```
git clone https://github.com/kassymkhanTJ/twitter_map.git
```
Create ```.env``` file:


```
cd twitter_map
cp .env.example .env
```

Create new database and configure in ```.env``` and `config/database.php` files

Run migrations:


```php artisan migrate```

Start server:


```php artisan serve```
